package com.example.spacelaunches.Data;

import static android.content.Context.MODE_PRIVATE;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.ImageView;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

@Entity
public class Image {

    @PrimaryKey(autoGenerate = true)
    public long id;
    public String path;

    public static void setImageDrawable(Context context, ImageView imageView, String path) {
        //Images are all loaded in the main thread. This is a small performance issue.
        //loading the images in a thread an rendering it on the ui thread will speed things up a bit.
        //but there are graphics issues with the current way that lists are populated.
        try {
            FileInputStream fis = context.openFileInput(path);
            Drawable drawable = Drawable.createFromStream(fis, null);
            imageView.setImageDrawable(drawable);
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String saveToInternalStorage(Context context, Uri uri) {
        String filename = UUID.randomUUID().toString();
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            FileOutputStream fos = context.openFileOutput(filename, MODE_PRIVATE);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return filename;
    }


}
