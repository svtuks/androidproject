package com.example.spacelaunches.Data;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Launch {

    @PrimaryKey(autoGenerate = true)
    public long id;

    public String name;
    public String providerName;
    public String vehicleName;
    public String winOpen;
    public String date;
    public int time;

    @NonNull
    @Override
    public String toString() {
        return "Launch{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", providerName='" + providerName + '\'' +
                ", vehicleName='" + vehicleName + '\'' +
                ", winOpen='" + winOpen + '\'' +
                ", date='" + date + '\'' +
                ", time=" + time +
                '}';
    }
}
