package com.example.spacelaunches.Data;

import static androidx.room.ForeignKey.CASCADE;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;

@Entity(foreignKeys = {
        @ForeignKey(entity = Image.class, parentColumns = {"id"}, childColumns = {"imageId"}),
        @ForeignKey(entity = Launch.class, parentColumns = {"id"}, childColumns = {"launchId"}, onDelete = CASCADE)},
        primaryKeys = {"imageId", "launchId"})
public class LaunchImage {
    public long imageId;
    public long launchId;

    @NonNull
    @Override
    public String toString() {
        return "LaunchImage{" +
                "imageId=" + imageId +
                ", launchId=" + launchId +
                '}';
    }
}
