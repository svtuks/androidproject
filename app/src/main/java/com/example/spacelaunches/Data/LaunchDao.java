package com.example.spacelaunches.Data;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface LaunchDao {

    class LaunchWithImagePath extends Launch {
        public String path;

        @NonNull
        @Override
        public String toString() {
            return "LaunchWithImagePath{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", providerName='" + providerName + '\'' +
                    ", vehicleName='" + vehicleName + '\'' +
                    ", winOpen='" + winOpen + '\'' +
                    ", date='" + date + '\'' +
                    ", time=" + time +
                    ", path='" + path + '\'' +
                    '}';
        }
    }

    @Insert
    void addImage(Image image);

    @Insert
    long addLaunch(Launch launch);

    @Insert
    void addLaunchImage(LaunchImage launchImage);

    @Delete
    void deleteLaunch(Launch launch);

    @Query("SELECT launch.*, image.path FROM Launch LEFT JOIN launchimage ON launchimage.launchId=launch.id LEFT JOIN Image ON image.id=launchimage.imageId GROUP BY launch.id")
    LiveData<List<LaunchWithImagePath>> getLaunches();

    @Update
    void updateLaunch(Launch launch);

    @Delete
    void deleteLaunchImage(LaunchImage launchImage);

    @Query("SELECT * FROM LAUNCHIMAGE WHERE launchId=:id")
    LaunchImage getLaunchImage(long id);

    @Query("SELECT * FROM Image")
    LiveData<List<Image>> getImages();

    @Query("SELECT launch.*, image.path FROM Launch LEFT JOIN launchimage ON launchimage.launchId=launch.id LEFT JOIN Image ON image.id=launchimage.imageId WHERE launch.id=:id GROUP BY launch.id")
    LaunchWithImagePath getLaunchWithImagePath(long id);

    @Query("SELECT launch.*, image.path FROM Launch LEFT JOIN launchimage ON launchimage.launchId=launch.id LEFT JOIN Image ON image.id=launchimage.imageId WHERE launch.id=:id GROUP BY launch.id")
    LiveData<LaunchWithImagePath> getLiveLaunchWithImage(long id);
}
