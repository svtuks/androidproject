package com.example.spacelaunches.Data;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;

@androidx.room.Database(entities = {Launch.class, Image.class, LaunchImage.class}, version = 14)
public abstract class AppDatabase extends RoomDatabase {

    public abstract LaunchDao launchDao();

    private static AppDatabase instance;

    static synchronized public AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "launches").fallbackToDestructiveMigration().build();
        }
        return instance;
    }

}
