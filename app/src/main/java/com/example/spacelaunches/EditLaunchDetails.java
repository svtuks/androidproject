
package com.example.spacelaunches;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.spacelaunches.Data.AppDatabase;
import com.example.spacelaunches.Data.Image;
import com.example.spacelaunches.Data.Launch;
import com.example.spacelaunches.Data.LaunchDao;
import com.example.spacelaunches.Data.LaunchImage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class EditLaunchDetails extends AppCompatActivity {

    private LaunchDao.LaunchWithImagePath launch;
    private LaunchDao launchDao;
    private ImageView launchImageView;
    private Date date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_launch_details);

        AppDatabase appDatabase = AppDatabase.getInstance(this);
        launchDao = appDatabase.launchDao();

        date = new Date();

        Intent intent = getIntent();
        long id = intent.getLongExtra("id", -1);
        if (id < 0) {
            new Thread(() -> {
                Launch launch = new Launch();
                long newId = launchDao.addLaunch(launch);
                runOnUiThread(() -> populateViews(newId));
            }).start();
        } else {
            populateViews(id);
        }
    }

    private void populateViews(long id) {
        launchImageView = findViewById(R.id.imageView_launchImage);

        Button buttonSave = findViewById(R.id.button_edit_save);
        Button buttonAddImage = findViewById(R.id.button_addImage);

        CalendarView calendarView = findViewById(R.id.calendarView);

        calendarView.setOnDateChangeListener((calendarView1, year, month, day) ->
                date = new GregorianCalendar(year, month, day).getTime()
        );

        SimpleDateFormat fmt = new SimpleDateFormat("MMM dd", Locale.ENGLISH);

        EditText editTextName = findViewById(R.id.editTextName);
        EditText editTextProvider = findViewById(R.id.editTextTextProvider);
        EditText editTextVehicle = findViewById(R.id.editTextTextVehicle);
        EditText editTextWindow = findViewById(R.id.editTextTextWindow);

        new Thread(() -> {
            launch = launchDao.getLaunchWithImagePath(id);

            runOnUiThread(() -> {
                editTextName.setText(launch.name);
                editTextProvider.setText(launch.providerName);
                editTextVehicle.setText(launch.vehicleName);
                editTextWindow.setText(launch.winOpen);

                try {
                    Calendar calendar = Calendar.getInstance();
                    int currentYear = calendar.get(Calendar.YEAR);
                    if (launch.date != null) {
                        date = fmt.parse(launch.date);
                        calendar.setTime(date);
                        calendar.set(Calendar.YEAR, currentYear);
                        calendarView.setDate(calendar.getTimeInMillis());
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (launch.path != null) {
                    Image.setImageDrawable(EditLaunchDetails.this, launchImageView, launch.path);
                }
            });
        }).start();

        launchDao.getLiveLaunchWithImage(id).observe(this, launchWithImagePath -> {
            if (launchWithImagePath.path != null) {
                launch.path = launchWithImagePath.path;
                Image.setImageDrawable(EditLaunchDetails.this, launchImageView, launchWithImagePath.path);
            }
        });

        buttonSave.setOnClickListener(view -> {
            launch.name = editTextName.getText().toString();
            launch.providerName = editTextProvider.getText().toString();
            launch.vehicleName = editTextVehicle.getText().toString();
            launch.winOpen = editTextWindow.getText().toString();
            launch.date = fmt.format(date);

            new Thread(() -> {
                launchDao.updateLaunch(launch);
                finish();
            }).start();
        });

        buttonAddImage.setOnClickListener(view -> {
            Intent intentImage = new Intent(EditLaunchDetails.this, SelectImage.class);
            startActivityForResult(intentImage, 0);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 0 && resultCode == 0) {
            if (data != null) {
                long imageId = data.getLongExtra("id", -1);
                new Thread(() -> {
                    if (launch.path != null) {
                        LaunchImage launchImage = launchDao.getLaunchImage(launch.id);
                        launchDao.deleteLaunchImage(launchImage);
                    }
                    LaunchImage launchImage = new LaunchImage();
                    launchImage.imageId = imageId;
                    launchImage.launchId = launch.id;
                    launchDao.addLaunchImage(launchImage);

                }).start();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}