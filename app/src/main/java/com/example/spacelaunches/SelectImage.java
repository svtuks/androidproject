package com.example.spacelaunches;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ImageButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.spacelaunches.Data.AppDatabase;
import com.example.spacelaunches.Data.Image;
import com.example.spacelaunches.Data.LaunchDao;

public class SelectImage extends AppCompatActivity {

    private final int photoRequest = 0;
    private LaunchDao launchDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_image);

        AppDatabase appDatabase = AppDatabase.getInstance(this);
        launchDao = appDatabase.launchDao();

        GridView gridView = findViewById(R.id.imageGrid);
        gridView.setOnItemClickListener((adapterView, view, i, l) -> {
            Intent intent = new Intent();
            Image image = (Image) adapterView.getAdapter().getItem(i);
            intent.putExtra("id", image.id);
            setResult(0, intent);
            finish();
        });

        launchDao.getImages().observe(this, images ->
                gridView.setAdapter(new ImageAdapter(SelectImage.this, images))
        );

        ImageButton addPhoto = findViewById(R.id.imageButton_add);
        addPhoto.setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, photoRequest);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == photoRequest) {
            if (data != null) {
                Uri uri = data.getData();
                String filename = Image.saveToInternalStorage(SelectImage.this, uri);
                Image image = new Image();
                image.path = filename;

                new Thread(() -> launchDao.addImage(image)).start();
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}