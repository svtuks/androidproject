package com.example.spacelaunches;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.spacelaunches.Data.Image;
import com.example.spacelaunches.Data.LaunchDao;

import java.util.List;

public class LaunchAdapter extends ArrayAdapter<LaunchDao.LaunchWithImagePath> {
    public LaunchAdapter(@NonNull Context context, @NonNull List<LaunchDao.LaunchWithImagePath> objects) {
        super(context, R.layout.launch_list_item, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = View.inflate(getContext(), R.layout.launch_list_item, null);
        }

        LaunchDao.LaunchWithImagePath launch = getItem(position);
        TextView textViewName = convertView.findViewById(R.id.item_name);
        TextView textViewDate = convertView.findViewById(R.id.item_date);
        ImageView imageView = convertView.findViewById(R.id.imageView_list);

        textViewName.setText(launch.name);
        textViewDate.setText(launch.date);

        if (launch.path != null) {
            Image.setImageDrawable(convertView.getContext(), imageView, launch.path);
        }

        return convertView;
    }
}
