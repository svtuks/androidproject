
package com.example.spacelaunches;

import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.spacelaunches.Data.AppDatabase;
import com.example.spacelaunches.Data.Image;
import com.example.spacelaunches.Data.LaunchDao;
import com.example.spacelaunches.Data.TempLaunches;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class LaunchDescription extends AppCompatActivity {

    private LaunchDao.LaunchWithImagePath launch;
    private boolean fromDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_description);

        AppDatabase appDatabase = AppDatabase.getInstance(this);
        LaunchDao launchDao = appDatabase.launchDao();

        Intent intent = getIntent();
        fromDatabase = intent.getBooleanExtra("database", false);
        long id = intent.getLongExtra("id", -1);
        if (id < 0) {
            finish();
            return;
        }

        Button button = findViewById(R.id.button_description);
        button.setOnClickListener(view -> {
            if (fromDatabase) {
                new Thread(() -> {
                    launchDao.deleteLaunch(launch);
                    finish();
                }).start();
            } else {
                new Thread(() -> {
                    long launchId = launchDao.addLaunch(launch);
                    Intent launchIntent = new Intent(LaunchDescription.this, LaunchDescription.class);
                    launchIntent.putExtra("id", launchId);
                    launchIntent.putExtra("database", true);
                    startActivity(launchIntent);
                    finish();
                }
                ).start();
                Toast.makeText(LaunchDescription.this, "Saved", Toast.LENGTH_LONG).show();
            }
        });

        Button addCalendar = findViewById(R.id.button_addCalendar);
        addCalendar.setOnClickListener(view -> {

            try {
                SimpleDateFormat fmt = new SimpleDateFormat("MMM dd", Locale.ENGLISH);
                Date date = fmt.parse(launch.date);
                Calendar calendar = Calendar.getInstance();
                int currentYear = calendar.get(Calendar.YEAR);
                calendar.setTime(date);
                calendar.set(Calendar.YEAR, currentYear);
                calendar.add(Calendar.DAY_OF_MONTH, 1);

                Intent calendarIntent = new Intent(Intent.ACTION_INSERT);
                calendarIntent.setData(CalendarContract.Events.CONTENT_URI);
                calendarIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, calendar.getTimeInMillis());
                calendarIntent.putExtra(CalendarContract.Events.ALL_DAY, true);
                calendarIntent.putExtra(CalendarContract.Events.TITLE, launch.name);
                calendarIntent.putExtra(CalendarContract.Events.DESCRIPTION, launch.winOpen);
                startActivity(calendarIntent);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });


        if (fromDatabase) {
            button.setText(getText(R.string.delete));
            launchDao.getLiveLaunchWithImage(id).observe(this, currentLaunch -> {
                if (currentLaunch != null) {
                    launch = currentLaunch;
                    updateViews();
                }
            });

        } else {
            launch = TempLaunches.launches.get((int) id);
            updateViews();
        }

    }

    void updateViews() {
        setTitle(launch.name);

        TextView textViewProviderName = findViewById(R.id.textView_providerName);
        TextView textViewVehicleName = findViewById(R.id.textView_vehicleName);
        TextView textViewWindow = findViewById(R.id.textView_window);
        TextView textViewDate = findViewById(R.id.textView_date);
        ImageView imageViewLaunch = findViewById(R.id.imageView_launch);

        textViewProviderName.setText(launch.providerName);
        textViewVehicleName.setText(launch.vehicleName);
        textViewWindow.setText(launch.winOpen);
        textViewDate.setText(launch.date);

        if (launch.path != null) {
            Image.setImageDrawable(LaunchDescription.this, imageViewLaunch, launch.path);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (launch.id > 0) {
            getMenuInflater().inflate(R.menu.menu_description, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.button_description_edit) {
            Intent intent = new Intent(LaunchDescription.this, EditLaunchDetails.class);
            intent.putExtra("id", launch.id);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}