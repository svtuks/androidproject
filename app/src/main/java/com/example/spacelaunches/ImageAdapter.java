package com.example.spacelaunches;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.spacelaunches.Data.Image;

import java.util.List;

public class ImageAdapter extends ArrayAdapter<Image> {

    public ImageAdapter(@NonNull Context context, @NonNull List<Image> objects) {
        super(context, R.layout.layout_image_item, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null){
            convertView = View.inflate(getContext(), R.layout.layout_image_item, null);
        }

        Image image = getItem(position);
        ImageView imageView = convertView.findViewById(R.id.imageItem);
        Image.setImageDrawable(getContext(), imageView, image.path);

        return  convertView;
    }
}
