package com.example.spacelaunches;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.spacelaunches.Data.LaunchDao;
import com.example.spacelaunches.Data.TempLaunches;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private SwipeRefreshLayout swipeRefreshLayout;
    private RequestQueue requestQueue;
    private ListView listView;
    private float acelVal;
    private float acelLast;
    private float shake;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        swipeRefreshLayout = findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.setOnRefreshListener(this::refresh);

        listView = findViewById(R.id.launchList);
        listView.setOnItemClickListener((adapterView, view, i, l) -> {
            Intent intent = new Intent(MainActivity.this, LaunchDescription.class);
            intent.putExtra("id", (long) i);
            startActivity(intent);
        });

        requestQueue = Volley.newRequestQueue(this);
        refresh();

        SensorManager sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sm.registerListener(sensorEventListener, sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        acelVal = SensorManager.GRAVITY_EARTH;
        acelLast = SensorManager.GRAVITY_EARTH;
        shake = 0.00f;
    }


    SensorEventListener sensorEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];
            acelLast = acelVal;
            acelVal = (float) Math.sqrt((double) (x * x) + (y * y) + (z * z));
            float delta = acelVal - acelLast;
            shake = shake * 0.9f + delta;
            if (shake > 8) {
                ImageView yeet = findViewById(R.id.yeet);
                yeet.setVisibility(View.VISIBLE);
                AnimatorSet animatorSet = new AnimatorSet();
                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(yeet, "translationY", -5000f);
                objectAnimator.setDuration(5000);
                ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(yeet, "translationY", 0);
                objectAnimator1.setDuration(0);
                animatorSet.playSequentially(objectAnimator, objectAnimator1);
                animatorSet.start();
                animatorSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        yeet.setVisibility(View.INVISIBLE);
                        super.onAnimationEnd(animation);
                    }
                });
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };

    void refresh() {
        String url = "https://fdo.rocketlaunch.live/json/launches/next/5";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, response -> {
            JSONArray result = response.optJSONArray("result");
            if (result != null) {
                TempLaunches.launches = new ArrayList<>();
                for (int i = 0; i < result.length(); i++) {
                    JSONObject launchData = result.optJSONObject(i);
                    String name = launchData.optString("name");
                    JSONObject provider = launchData.optJSONObject("provider");
                    String providerName = provider.optString("name");
                    JSONObject vehicle = launchData.optJSONObject("vehicle");
                    String vehicleName = vehicle.optString("name");
                    String winOpen = launchData.optString("win_open");
                    String date = launchData.optString("date_str");
                    int time = launchData.optInt("sort_date");

                    LaunchDao.LaunchWithImagePath launch = new LaunchDao.LaunchWithImagePath();
                    launch.name = name;
                    launch.providerName = providerName;
                    launch.vehicleName = vehicleName;
                    launch.winOpen = winOpen;
                    launch.date = date;
                    launch.time = time;

                    TempLaunches.launches.add(launch);
                }
                updateList();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, error -> {
            Toast.makeText(MainActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            error.printStackTrace();
        });
        requestQueue.add(jsonObjectRequest);
    }

    void updateList() {
        listView.setAdapter(new LaunchAdapter(MainActivity.this, TempLaunches.launches));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_refresh) {
            swipeRefreshLayout.setRefreshing(true);
            refresh();
        } else if (item.getItemId() == R.id.menu_saved_items) {
            Intent intent = new Intent(MainActivity.this, SavedLaunches.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}