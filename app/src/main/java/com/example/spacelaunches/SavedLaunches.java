package com.example.spacelaunches;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.spacelaunches.Data.AppDatabase;
import com.example.spacelaunches.Data.Launch;
import com.example.spacelaunches.Data.LaunchDao;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class SavedLaunches extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_launches);

        AppDatabase appDatabase = AppDatabase.getInstance(this);
        LaunchDao launchDao = appDatabase.launchDao();

        ListView listSavedLaunches = findViewById(R.id.list_savedLaunches);

        listSavedLaunches.setOnItemClickListener((adapterView, view, i, l) -> {
            Launch launch = (Launch) adapterView.getAdapter().getItem(i);
            Intent intent = new Intent(SavedLaunches.this, LaunchDescription.class);
            intent.putExtra("id", launch.id);
            intent.putExtra("database", true);
            startActivity(intent);
        });

        launchDao.getLaunches().observe(this, launches ->
                listSavedLaunches.setAdapter(new LaunchAdapter(SavedLaunches.this, launches))
        );

        setTitle("Saved launches");

        FloatingActionButton button = findViewById(R.id.floatingActionButton_add);
        button.setOnClickListener(view -> {
            Intent intent = new Intent(SavedLaunches.this, EditLaunchDetails.class);
            startActivity(intent);
        });
    }
}